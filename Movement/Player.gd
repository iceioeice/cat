extends KinematicBody2D

#export (int) var speed = 200
#
#var velocity = Vector2()
#
#func get_input():
#	velocity = Vector2()
#	if Input.is_action_pressed('right'):
#		velocity.x += 1
#	if Input.is_action_pressed('left'):
#		velocity.x -= 1
#	if Input.is_action_pressed('down'):
#		velocity.y += 1
#	if Input.is_action_pressed('up'):
#		velocity.y -= 1
#	velocity = velocity.normalized() * speed
#
#func _physics_process(delta):
#	get_input()
#	move_and_slide(velocity)

#Rotate - move

#export (int) var speed = 200
#export (float) var rotation_speed = 1.5
#
#var velocity = Vector2()
#var rotation_dir = 0
#
#func get_input():
#	rotation_dir = 0
#	velocity = Vector2()
#	if Input.is_action_pressed('right'):
#		rotation_dir += 1
#	if Input.is_action_pressed('left'):
#		rotation_dir -= 1
#	if Input.is_action_pressed('down'):
#		velocity = Vector2(-speed, 0).rotated(rotation)
#	if Input.is_action_pressed('up'):
#		velocity = Vector2(speed, 0).rotated(rotation)
#
#func _physics_process(delta):
#	get_input()
#	rotation += rotation_dir * rotation_speed * delta
#	move_and_slide(velocity)


#Click - Move

export (int) var speed = 200

var target = Vector2()
var velocity = Vector2()

func _input(event):
	if event.is_action_pressed('click'):
		target = get_global_mouse_position()

func _physics_process(delta):
	velocity = (target - position).normalized() * speed
	# rotation = velocity.angle()
	if (target - position).length() > 5:
		move_and_slide(velocity)
